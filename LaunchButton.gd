extends Button

onready var changelog = get_node("../../Panel/RichTextLabel")

var http_exe = "http://eyporage.net/PokemonMMO/wtfmon.exe"
var http_version = "http://eyporage.net/PokemonMMO/version.txt"
var http_changelog = "http://eyporage.net/PokemonMMO/changelog.txt"

var path_exe = "user://wtfmon.exe"
var path_version = "user://version.txt"
var path_changelog = "user://changelog.txt"

var http_request: HTTPRequest
var click_pos = Vector2.ZERO


func _process(_delta):
	if Input.is_action_just_pressed("click"):
		click_pos = get_global_mouse_position()
	if Input.is_action_pressed("click"):
		OS.set_window_position(
			OS.window_position +
			get_global_mouse_position() - 
			click_pos
		)

func _ready():
	_verify_gamefiles()
	_set_changelog()
	self.disabled = true

func _set_changelog():
	var file = File.new()
	var err = file.open(path_changelog, File.READ)
	if err == OK:
		var text = file.get_as_text()
		changelog.text = text
		file.close()
	

func file_exists(path: String) -> bool:
	var dir = Directory.new()
	return dir.file_exists(path)
	

func _verify_gamefiles():
	
	if file_exists(path_exe) && file_exists(path_version) && file_exists(path_changelog):
		_download_file(http_version,path_version, true, "version")
	else:
		_check_integrity()

func _download_file(link: String, path:String, just_version:bool, flag:String):
	
	http_request = HTTPRequest.new()
	add_child(http_request)
	
	self.text = "Downloading " + str(path.get_file())
	http_request.connect("request_completed", self, "_install_file", [path, just_version,flag])
	
	var error = http_request.request_raw(link)
	if error != OK:
		self.text = "Download Error " + str(error)
		return
	
func _install_file(_result, _response_code, _headers, body, path, just_version:bool, flag:String):
	if just_version:
		var new_version = str(body.get_string_from_utf8())
		_compare_version(new_version)
		return
	
	var dir = Directory.new()
	dir.remove(path)
	
	var file = File.new()
	file.open(path, File.WRITE)
	file.store_buffer(body)
	file.close()
	if flag == "changelog":
		_set_changelog()
	_check_integrity()
	
func _check_integrity():
	var download_succeeded = false
	if !file_exists(path_exe):
		_download_file(http_exe, path_exe, false, "exe")
		print ("no executable")
		return
		
	if !file_exists(path_version):
		_download_file(http_version, path_version, false, "version")
		print ("no version.txt")
		return
	
	if !file_exists(path_changelog):
		_download_file(http_changelog, path_changelog, false, "changelog")
		print ("no changelog.txt")
		return
	
	self.text = "Start"
	self.disabled = false
	
func _compare_version(new_version):
	var file = File.new()
	file.open(path_version, File.READ)
	var cur_version = file.get_as_text()
	file.close()
	print(new_version)
	print(cur_version)
	if int(new_version) > int(cur_version):
		print("wdf")
		var dir = Directory.new()
		dir.remove(path_version)
		dir.remove(path_exe)
		dir.remove(path_changelog)
	_check_integrity()

func _start_game():
	OS.shell_open(OS.get_user_data_dir() + "/wtfmon.exe")

func _on_LaunchButton_pressed():
	_start_game()


func _on_Button_pressed():
	get_tree().quit()
