extends Control





onready var gameserver = get_node("Background/MarginContainer/VBoxContainer/Panel/HBoxContainer/VBoxContainer/Status/Game/Value")
onready var authserver = get_node("Background/MarginContainer/VBoxContainer/Panel/HBoxContainer/VBoxContainer/Status/Auth/Value")
onready var gatewayserver = get_node("Background/MarginContainer/VBoxContainer/Panel/HBoxContainer/VBoxContainer/Status/Gateway/Value")
onready var webserver = get_node("Background/MarginContainer/VBoxContainer/Panel/HBoxContainer/VBoxContainer/Status/File/Value")

var cert = load("res://X509_Certificate.crt")

## GAMESERVER
var network
var http_request: HTTPRequest

var gtick = 0
var atick = 0
var gwtick = 0


func _ready():
	checkConnectionStatus()
	
	
	
func checkConnectionStatus():
	network = NetworkedMultiplayerENet.new()

	ConnectToServeGame()
	yield(get_tree().create_timer(0.25),"timeout")
	if (network.get_connection_status() == NetworkedMultiplayerENet.CONNECTION_DISCONNECTED):
		gameserver.set_text("offline")
		gameserver.add_color_override("font_color", Color(1,0,0))
	elif(network.get_connection_status() == NetworkedMultiplayerENet.CONNECTION_CONNECTED):
		gameserver.set_text("online")
		gameserver.add_color_override("font_color", Color(0,1,0))
	elif(network.get_connection_status() == NetworkedMultiplayerENet.CONNECTION_CONNECTING):
		gameserver.set_text("connecting")
		gameserver.add_color_override("font_color", Color(1,1,0))
		gtick = gtick + 1
		if gtick > 1 :
			gameserver.set_text("offline")
			gameserver.add_color_override("font_color", Color(1,0,0))
	else:
		gameserver.set_text("online")
		gameserver.add_color_override("font_color", Color(0,1,0))
	
	
#	network = NetworkedMultiplayerENet.new()
#
#	ConnectToServeGateway()
#	yield(get_tree().create_timer(0.25),"timeout")
#
#	if (network.get_connection_status() == NetworkedMultiplayerENet.CONNECTION_DISCONNECTED):
#		gatewayserver.set_text("offline")
#		gatewayserver.add_color_override("font_color", Color(1,0,0))
#	elif(network.get_connection_status() == NetworkedMultiplayerENet.CONNECTION_CONNECTED):
#		gatewayserver.set_text("online")
#		gatewayserver.add_color_override("font_color", Color(0,1,0))
#	elif(network.get_connection_status() == NetworkedMultiplayerENet.CONNECTION_CONNECTING):
#		gatewayserver.set_text("connecting")
#		gatewayserver.add_color_override("font_color", Color(1,1,0))
#		gwtick = gwtick + 1
#		if gwtick > 1 :
#			gatewayserver.set_text("offline")
#			gatewayserver.add_color_override("font_color", Color(1,0,0))
#	else:
#		gatewayserver.set_text("online")
#		gatewayserver.add_color_override("font_color", Color(0,1,0))
#
#	gatewayserver.set_text("online")
#	gatewayserver.add_color_override("font_color", Color(0,1,0))

	network = NetworkedMultiplayerENet.new()

	ConnectToServeAuth()
	yield(get_tree().create_timer(0.25),"timeout")
	if (network.get_connection_status() == NetworkedMultiplayerENet.CONNECTION_DISCONNECTED):
		authserver.set_text("offline")
		authserver.add_color_override("font_color", Color(1,0,0))
	elif(network.get_connection_status() == NetworkedMultiplayerENet.CONNECTION_CONNECTED):
		authserver.set_text("online")
		authserver.add_color_override("font_color", Color(0,1,0))
	elif(network.get_connection_status() == NetworkedMultiplayerENet.CONNECTION_CONNECTING):
		authserver.set_text("connecting")
		authserver.add_color_override("font_color", Color(1,1,0))
		atick = atick + 1
		if atick > 1 :
			authserver.set_text("offline")
			authserver.add_color_override("font_color", Color(1,0,0))
	else:
		authserver.set_text("online")
		authserver.add_color_override("font_color", Color(0,1,0))
		
	
	
	network = NetworkedMultiplayerENet.new()
	ConnectToFileServer()
	
func ConnectToServeGame():
	var ip = "23.88.120.66"
	var port = 1909
	
	var err = network.create_client(ip,port)
	get_tree().set_network_peer(network)


## Gatewayserver

func ConnectToServeGateway():
	var ip = "23.88.120.66"
	var port = 1910
	network = NetworkedMultiplayerENet.new()
	var gateway_api = MultiplayerAPI.new()
	network.set_dtls_enabled(true)
	network.set_dtls_verify_enabled(false)
	network.set_dtls_certificate(cert)
	
	var err = network.create_client(ip,port)
	if err:
		return false
	set_custom_multiplayer(gateway_api)
	custom_multiplayer.set_root_node(self)
	custom_multiplayer.set_network_peer(network)
	


## AUTHSERVER
	
func ConnectToServeAuth():
	var ip = "23.88.120.66"
	var port = 1911

	var err = network.create_client(ip,port)
	get_tree().set_network_peer(network)
	
	

	
func ConnectToFileServer():
	http_request = HTTPRequest.new()
	add_child(http_request)
	http_request.connect("request_completed", self, "_retrieve_response")
	
	var error = http_request.request_raw("http://eyporage.net")
	if error != OK:
		webserver.set_text("offline")
		webserver.add_color_override("font_color", Color(1,0,0))
	
func _retrieve_response(_result, _response_code, _headers, body):
	if _response_code == 200:
		webserver.set_text("online")
		webserver.add_color_override("font_color", Color(0,1,0))
	else:
		webserver.set_text("offline")
		webserver.add_color_override("font_color", Color(1,0,0))


func _on_StatusTimer_timeout():
	checkConnectionStatus()
